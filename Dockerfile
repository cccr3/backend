FROM amazoncorretto:11

ARG JAR_FILE
ARG DB_URL
ARG DB_USER
ARG DB_PW

RUN yum install -y glibc-langpack-ko
ENV LANG ko_KR.UTF8
ENV LC_ALL ko_KR.UTF8
RUN ln -sf /usr/share/zoneinfo/Asia/Seoul /etc/localtime

VOLUME /tmp

COPY ${JAR_FILE} app.jar

ENTRYPOINT ["java", \
"-jar", \
"/app.jar"]